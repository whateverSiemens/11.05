/********************************************************************************
** Form generated from reading UI file 'dspmainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DSPMAINWINDOW_H
#define UI_DSPMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_DSPMainWindow
{
public:
    QWidget *centralWidget;
    QCustomPlot *plot;
    QLineEdit *lineEditFrequency2;
    QLineEdit *lineEditFrequency5;
    QLineEdit *lineEditFrequency4;
    QLineEdit *lineEditFrequency1;
    QLineEdit *lineEditFrequency3;
    QPushButton *btnPlot;
    QCustomPlot *plot_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *DSPMainWindow)
    {
        if (DSPMainWindow->objectName().isEmpty())
            DSPMainWindow->setObjectName(QStringLiteral("DSPMainWindow"));
        DSPMainWindow->resize(1200, 700);
        centralWidget = new QWidget(DSPMainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        plot = new QCustomPlot(centralWidget);
        plot->setObjectName(QStringLiteral("plot"));
        plot->setGeometry(QRect(9, 38, 1180, 281));
        lineEditFrequency2 = new QLineEdit(centralWidget);
        lineEditFrequency2->setObjectName(QStringLiteral("lineEditFrequency2"));
        lineEditFrequency2->setGeometry(QRect(220, 10, 51, 20));
        lineEditFrequency5 = new QLineEdit(centralWidget);
        lineEditFrequency5->setObjectName(QStringLiteral("lineEditFrequency5"));
        lineEditFrequency5->setGeometry(QRect(450, 10, 51, 20));
        lineEditFrequency4 = new QLineEdit(centralWidget);
        lineEditFrequency4->setObjectName(QStringLiteral("lineEditFrequency4"));
        lineEditFrequency4->setGeometry(QRect(370, 10, 51, 20));
        lineEditFrequency1 = new QLineEdit(centralWidget);
        lineEditFrequency1->setObjectName(QStringLiteral("lineEditFrequency1"));
        lineEditFrequency1->setGeometry(QRect(140, 10, 51, 20));
        lineEditFrequency3 = new QLineEdit(centralWidget);
        lineEditFrequency3->setObjectName(QStringLiteral("lineEditFrequency3"));
        lineEditFrequency3->setGeometry(QRect(290, 10, 51, 20));
        btnPlot = new QPushButton(centralWidget);
        btnPlot->setObjectName(QStringLiteral("btnPlot"));
        btnPlot->setGeometry(QRect(550, 10, 75, 23));
        plot_2 = new QCustomPlot(centralWidget);
        plot_2->setObjectName(QStringLiteral("plot_2"));
        plot_2->setGeometry(QRect(10, 350, 1180, 281));
        DSPMainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(DSPMainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1200, 21));
        DSPMainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(DSPMainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        DSPMainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(DSPMainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        DSPMainWindow->setStatusBar(statusBar);

        retranslateUi(DSPMainWindow);

        QMetaObject::connectSlotsByName(DSPMainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *DSPMainWindow)
    {
        DSPMainWindow->setWindowTitle(QApplication::translate("DSPMainWindow", "DSPMainWindow", Q_NULLPTR));
        btnPlot->setText(QApplication::translate("DSPMainWindow", "PLOT!", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DSPMainWindow: public Ui_DSPMainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DSPMAINWINDOW_H
