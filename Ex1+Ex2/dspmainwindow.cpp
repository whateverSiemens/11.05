#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DSPMainWindow)
{
    ui->setupUi(this);

	QObject::connect(ui->btnPlot, SIGNAL(released()), this, SLOT(mf_btnPlot()));

	
}

DSPMainWindow::~DSPMainWindow()
{
    delete ui;
}


void DSPMainWindow::mf_btnPlot()
{
	QVector<double> xAxis(44100);
	double yAxis1, yAxis2, yAxis3, yAxis4, yAxis5;
	QVector<double> yAxisSum(44100);

	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
		yAxis1 = 1 * sin(2 * M_PI * ui->lineEditFrequency1->text().toDouble() * xAxis[i]);
		yAxis2 = 1 * sin(2 * M_PI * ui->lineEditFrequency2->text().toDouble() * xAxis[i]);
		yAxis3 = 1 * sin(2 * M_PI * ui->lineEditFrequency3->text().toDouble() * xAxis[i]);
		yAxis4 = 1 * sin(2 * M_PI * ui->lineEditFrequency4->text().toDouble() * xAxis[i]);
		yAxis5 = 1 * sin(2 * M_PI * ui->lineEditFrequency5->text().toDouble() * xAxis[i]);
		yAxisSum[i] = yAxis1 + yAxis2 + yAxis3 + yAxis4 + yAxis5;

	}

	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(xAxis, yAxisSum);
	ui->plot->xAxis->setRange(0,1);
	ui->plot->yAxis->setRange(-5, 5);
	ui->plot->replot();




	int FFTSize = 2048;
	double frequencyRes = 44100.0 / FFTSize;
	auto fft = Aquila::FftFactory::getFft(FFTSize);
	auto spectrum = fft->fft(yAxisSum.toStdVector().data());


	QVector<double> amplitude, frequency;
	int index = 0;
	for each(auto item in spectrum)
	{
		amplitude.push_back(pow(item.real(), 2) + pow(item.imag(), 2));
		frequency.push_back((index++)*frequencyRes);
	}

	ui->plot_2->addGraph();
	ui->plot_2->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
	ui->plot_2->graph(0)->setData(frequency, amplitude);
	ui->plot_2->xAxis->setRange(20, 20000);//se poate seta si de la frecventa minima la cea maxima introdusa
	ui->plot_2->yAxis->setRange(0, *std::max_element(amplitude.begin(), amplitude.end()));
	ui->plot_2->replot();
}
