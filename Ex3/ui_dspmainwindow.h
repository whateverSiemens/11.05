/********************************************************************************
** Form generated from reading UI file 'dspmainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DSPMAINWINDOW_H
#define UI_DSPMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_DSPMainWindow
{
public:
    QWidget *centralWidget;
    QCustomPlot *plot;
    QLineEdit *lineEditFrequency1;
    QPushButton *btnPlot;
    QCustomPlot *plot_2;
    QCustomPlot *plot_3;
    QLabel *label;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *DSPMainWindow)
    {
        if (DSPMainWindow->objectName().isEmpty())
            DSPMainWindow->setObjectName(QStringLiteral("DSPMainWindow"));
        DSPMainWindow->resize(1200, 700);
        centralWidget = new QWidget(DSPMainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        plot = new QCustomPlot(centralWidget);
        plot->setObjectName(QStringLiteral("plot"));
        plot->setGeometry(QRect(9, 38, 1180, 171));
        lineEditFrequency1 = new QLineEdit(centralWidget);
        lineEditFrequency1->setObjectName(QStringLiteral("lineEditFrequency1"));
        lineEditFrequency1->setGeometry(QRect(140, 10, 81, 20));
        btnPlot = new QPushButton(centralWidget);
        btnPlot->setObjectName(QStringLiteral("btnPlot"));
        btnPlot->setGeometry(QRect(240, 10, 75, 23));
        plot_2 = new QCustomPlot(centralWidget);
        plot_2->setObjectName(QStringLiteral("plot_2"));
        plot_2->setGeometry(QRect(10, 250, 1180, 171));
        plot_3 = new QCustomPlot(centralWidget);
        plot_3->setObjectName(QStringLiteral("plot_3"));
        plot_3->setGeometry(QRect(10, 450, 1180, 171));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(80, 10, 47, 20));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        DSPMainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(DSPMainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1200, 21));
        DSPMainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(DSPMainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        DSPMainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(DSPMainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        DSPMainWindow->setStatusBar(statusBar);

        retranslateUi(DSPMainWindow);

        QMetaObject::connectSlotsByName(DSPMainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *DSPMainWindow)
    {
        DSPMainWindow->setWindowTitle(QApplication::translate("DSPMainWindow", "DSPMainWindow", Q_NULLPTR));
        btnPlot->setText(QApplication::translate("DSPMainWindow", "PLOT!", Q_NULLPTR));
        label->setText(QApplication::translate("DSPMainWindow", "kHz:", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DSPMainWindow: public Ui_DSPMainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DSPMAINWINDOW_H
