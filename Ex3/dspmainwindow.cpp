#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>
#include<aquila\source\generator\TriangleGenerator.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DSPMainWindow)
{
    ui->setupUi(this);

	QObject::connect(ui->btnPlot, SIGNAL(released()), this, SLOT(mf_btnPlot()));

	
}

DSPMainWindow::~DSPMainWindow()
{
    delete ui;
}


void DSPMainWindow::mf_btnPlot()
{
	long sample = 2400000;
	QVector<double> xAxis(sample), yAxisCarrier(sample),yAxisModulated(sample),yAxisAM(sample);
	
	double frequency = ui->lineEditFrequency1->text().toDouble() * 100; //Hz => kHz

	Aquila::TriangleGenerator generator(sample);
	generator.setWidth(0.5).setFrequency(frequency/100).setAmplitude(1).generate(sample);
	
	for (int i = 0; i < sample; i++)
		{
			xAxis[i] = i / (double)sample;			
			yAxisCarrier[i] =  1*sin(2 * M_PI * frequency * xAxis[i]);
			yAxisModulated[i] = generator.sample(i);//1 * sin(2 * M_PI * frequency/100 * xAxis[i]);
			yAxisAM[i] = (1 + generator.sample(i))*yAxisCarrier[i];
		}

	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(xAxis, yAxisModulated);
	ui->plot->xAxis->setRange(0, 0.005);
	ui->plot->yAxis->setRange(-1, 1);
	ui->plot->replot();


	ui->plot_2->addGraph();
	ui->plot_2->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot_2->graph(0)->setData(xAxis, yAxisCarrier);
	ui->plot_2->xAxis->setRange(0,0.005);
	ui->plot_2->yAxis->setRange(-1, 1);
	ui->plot_2->replot();

	ui->plot_3->addGraph();
	ui->plot_3->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot_3->graph(0)->setData(xAxis, yAxisAM);
	ui->plot_3->xAxis->setRange(0, 0.005);
	ui->plot_3->yAxis->setRange(-2, 2);
	ui->plot_3->replot();
}
